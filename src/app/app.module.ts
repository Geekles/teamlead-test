import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetailsComponent } from './pages/details/details.component';
import { ListComponent } from './pages/list/list.component';
import { HeaderComponent } from './layouts/header/header.component';
import { ProductsModule } from './pages/products/products.module';
import { ProductItemModule } from './commons/product-item/product-item.module';
import { HttpClientModule } from '@angular/common/http';
import { NgbCarouselModule, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CountdownModule } from 'ngx-countdown';
import { CarouselModule } from 'ngx-bootstrap/carousel';

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    ListComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProductsModule,
    ProductItemModule,
    HttpClientModule,
    NgbModule,
    CountdownModule,
    CarouselModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
