import { Component, OnInit, Input } from '@angular/core';
import { IProduct } from 'src/app/core/model/product.model';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  @Input() item: IProduct;

  constructor() { }

  ngOnInit(): void {
  }

}
