import { environment } from "./../../../environments/environment";

export class ProductsUtils {

    private static baseUrl: string = './assets/storage/' +  environment.apiProduct;

    public static getBaseUrl() : string {
        return this.baseUrl;
    }

}

