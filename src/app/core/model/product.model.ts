import { Time } from '@angular/common';
import { ISlide } from './slide.model';

export interface IProduct{
    id: number,
    name: string,
    price: number,
    timer: Time,
    picture: string,
    video: string,
    images: ISlide[]
}