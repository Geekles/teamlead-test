export interface ISlide{
    id: number,
    name: string,
    image: string
}