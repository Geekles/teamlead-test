import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IProduct } from '../model/product.model';
import { ProductsUtils } from "../utils/products-utils";

@Injectable({
  providedIn: 'root'
})
export class ProductsService  {

  product: IProduct;

  constructor(private IOClient: HttpClient) { }

  findAll() : Observable<IProduct[]> {
    return this.IOClient.get<IProduct[]>(ProductsUtils.getBaseUrl()); 
  } 

}
