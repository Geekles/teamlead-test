import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/core/services/products.service';
import { IProduct } from 'src/app/core/model/product.model';
import { CountdownConfig, CountdownComponent } from 'ngx-countdown';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  product: IProduct;
  config: CountdownConfig = {
    leftTime: 1800,
    format: "HH:mm:ss",
    demand: false
  };

  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;

  constructor(private route: ActivatedRoute, private service: ProductsService) {
    this.route.paramMap.subscribe(params => {
      this.service.findAll().subscribe(results => {
        results.forEach(item => {
          if(item.id == parseInt(params.get("id"))) {
            this.product = item;
          }
        })
      });
    })
  }

  ngOnInit(): void {
    if(this.countdown){
      this.countdown.begin();
    }
  }

  handleEvent(event) {
    console.log(event);
  }

}
