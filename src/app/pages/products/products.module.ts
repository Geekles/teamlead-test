import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from '../list/list.component';
import { DetailsComponent } from '../details/details.component';
import { ProductsComponent } from './products.component';

const routes: Routes = [
  {
    path: "",
    component: ProductsComponent,
    children: [
      {
        path: "",
        component: ListComponent
      },
      {
        path: "list",
        component: ListComponent
      },
      {
        path: "details/:id",
        component: DetailsComponent
      }
    ]
  }
]

@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  providers: []
})
export class ProductsModule { }
